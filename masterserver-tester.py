﻿from twisted.application import internet, service, app
from twisted.web import server, resource, static
from twisted.python import threadpool, log
from twisted.internet import reactor
from twisted.application.service import MultiService
from random import randint

from optparse import make_option
import sys
import os
import re

from pymaster.pymaster_module import MasterServerService
from pymaster.q3.server_list import Servers

ETL_MASTER_PORT = 27950
ETL_MASTER_API = 'http://dockerhost:8000'

from pymaster.q3.master_server import MasterServer
from twisted.internet import reactor
from fakeserver import FakeServerService

multi = MultiService()
delay = 0
ports = []
for i in range(0, 200):
    port = randint(27960, 40000)
    while port in ports:
        port = randint(27960, 40000)

    ports.append(port)
    fakeserver_service = FakeServerService(port, ETL_MASTER_API, delay)
    fakeserver_service.name += '_%d_%d'%(port, randint(0, 30000))
    fakeserver_service.setServiceParent(multi)

    delay += 0.05

    
application = service.Application("Masterserver Tester")
multi.setServiceParent(application)
multi.startService()

reactor.run()
