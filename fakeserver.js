/**
 * following: https://nodejs.org/docs/latest-v0.12.x/api/dgram.html#dgram_socket_send_buf_offset_length_port_address_callback
 * TODO
 */

var dgram = require('dgram');
var message = new Buffer("Some bytes");
var client = dgram.createSocket("udp4");
client.send(message, 0, message.length, 41234, "localhost", function(err) {
  client.close();
});