﻿from twisted.application import service
from twisted.application import internet
from twisted.python import log
from twisted.python import usage
from twisted.web import server
from twisted.internet.task import LoopingCall

from pymaster.q3.master_server import MasterServer
from pymaster.q3.server_list import Servers
from pymaster.q3.server_json_list import Root
from pymaster.q3.protocol.client import Q3Protocol
from random import randint, uniform
from twisted.internet import reactor

Q3MASTER_PORT = "27950"

class GameServer(Q3Protocol):

    def __init__(self, port, master_servers_address):
        Q3Protocol.__init__(self)
        self.port = port
        self.master_server_address = master_servers_address
        
    def loop_send_heartbeat(self):

        loop = LoopingCall(self.send_heartbeat)
        interval = 5
        delay = randint(5, 30)
        reactor.callLater(delay, self.send_heartbeat)

    def send_heartbeat(self):
        from urllib.request import urlopen
        response = urlopen(self.master_server_address + '/heartbeat/1337/%d/up' % self.port)
        html = response.read()

    def handle_getinfo(self, *args):
        (challenge, ip, port) = args
        challenge = challenge[1:] # trim the leading space

        delay = uniform(0.01, 1.0)

        reactor.callLater(delay, self.send_inforesponse, (ip, port), challenge)
        
    def send_inforesponse(self, addr, challenge):
        msg = 'infoResponse\n\\challenge\\%s\\version\\ET Legacy\\protocol\\84\\hostname\\^3Yellow ^7Snow\\serverload\\-1\\mapname\\oasis\\clients\\0\\humans\\0\\sv_maxclients\\20\\gametype\\4\\pure\\1\\game\\legacy\\friendlyFire\\1\\maxlives\\0\\needpass\\0\\gamename\\et\\g_antilag\\1\\weaprestrict\\100\ \balancedteams\\1'%(challenge)
        self.sendMessage(msg, addr)

class FakeServerService(service.Service):
    name = "Fake server"

    def __init__(self, port, master_servers_address, delay):
        self.port = port
        self.server = None
        self.master_servers_address = master_servers_address
        self.delay = delay

    def startService(self):
        from twisted.internet import reactor
        print("Starting %s Server on port %s" % (self.name, self.port))
        master_server = GameServer(self.port, self.master_servers_address)

        self.server = reactor.listenUDP(self.port, master_server)
        reactor.callLater(self.delay, self.server.protocol.loop_send_heartbeat)


